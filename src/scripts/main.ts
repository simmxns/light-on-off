const btn = <Element>document.querySelector('.btn')
const body = <HTMLBodyElement>document.querySelector('body')
const audio = <HTMLMediaElement>document.querySelector('#audio')

btn.addEventListener('click', (): void => {
	body.classList.toggle('on')
	audio.play()
})
